/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.message.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.butor.auth.common.AuthModel;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.BinResponseHandler;
import org.butor.json.service.Context;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.butor.message.common.message.Message;
import com.butor.message.common.message.MessageAttach;
import com.butor.message.common.message.MessageBinServices;
import com.butor.message.dao.message.MessageDao;
import com.butor.message.dao.message.SentMessageDao;
import com.google.common.io.Closeables;

public class DefaultMessageBinServices implements MessageBinServices {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private MessageDao messageDao;
	private SentMessageDao sentMessageDao;
	private AuthModel authModel;

	public DefaultMessageBinServices(
			MessageDao messageDao,
			SentMessageDao sentMessageDao,
			AuthModel authModel) {
		this.messageDao = checkNotNull(messageDao);
		this.sentMessageDao = checkNotNull(sentMessageDao);
		this.authModel = checkNotNull(authModel);
	}

	@Override
	public void downloadSentMessageAttach(Context ctx, final long messageId, final long fileId) {
		try {
			CommonRequestArgs cra = ctx.getRequest();
			// dao readMessage check access
			Message message = sentMessageDao.readMessage(messageId, cra);
			if (message == null)
				return;

			downloadAttach(ctx, message.getContentId(), fileId);
			
		} catch (Exception e) {
			logger.warn("Failed to download attachment", e);
			ApplicationException.exception(CommonMessageID.SERVICE_FAILURE.getMessage());
		}
	}
	@Override
	public void downloadMessageAttach(Context ctx, final long messageId, final long fileId) {
		try {
			CommonRequestArgs cra = ctx.getRequest();
			// dao readMessage check access
			Message message = messageDao.readMessage(messageId, cra);
			if (message == null)
				return;

			downloadAttach(ctx, message.getContentId(), fileId);
			
		} catch (Exception e) {
			logger.warn("Failed to download attachment", e);
			ApplicationException.exception(CommonMessageID.SERVICE_FAILURE.getMessage());
		}
	}
	
	private void downloadAttach(Context ctx, final long contentId, final long fileId) {
		try {
			CommonRequestArgs cra = ctx.getRequest();

			MessageAttach ma = sentMessageDao.readAttach(contentId, fileId, cra);
			if (ma == null)
				ApplicationException.exception(CommonMessageID.NOT_FOUND.getMessage());

			BinResponseHandler brh = (BinResponseHandler) ctx.getResponseHandler();

			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Content-disposition", "attachment; filename=\"" + ma.getFileName() +"\"");
			brh.setContentType(ma.getContentType(), headers);

			OutputStream os = brh.getOutputStream();
			
			String[] toks = ma.getContentBase64().split(";");
			if (toks.length==1) {
				os.write(Base64.getDecoder().decode(toks[0]));
			} else {
				toks = toks[1].split(",");
				os.write(Base64.getDecoder().decode(toks[1]));
			}
			
			Closeables.close(os, true);
			
		} catch (IOException e) {
			logger.warn("Failed to download attachment", e);
			ApplicationException.exception(CommonMessageID.SERVICE_FAILURE.getMessage());
		}
	}
}
