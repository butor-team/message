/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.message = {
	'tr' : function(txt) {
		return App.tr(txt, 'message');
	},
	formatAttachSize : function(size) {
		var sizes = ['B', 'K','M','G','T'];
		if (size == 0) return '0 B';
		var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
		return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
	},
	fileToBase64 : function(file, handler) {
		var dfr =  $.Deferred();
		
		if (handler) {
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				handler && handler(reader.result);
				dfr.resolve();
			};
			reader.onerror = function (error) {
				console.log('Error: ', error);
				handler && handler(null);
				dfr.resolve();
			};
		}
		
		return dfr.promise();
	}
};

butor.message.AppLoader = butor.message.AppLoader || butor.AppLoader.define({
	construct : function() {
		var debugModules = [
			'/message/res/js/portalApp.js',
			'/message/res/js/message.js',
			'/message/res/js/messageDlg.js',
			'/message/res/js/bundles.js',
			'/message/res/js/inbox.js',
			'/message/res/js/sent.js'
		];
		
		var modules = ['/message/res/js/message-all.js'];
		
		if (App.hasAccess('message','sendMessage')) {
			modules.push('/message/res/trumbowyg/trumbowyg.min.js');
			modules.push('/message/res/trumbowyg/langs/fr.min.js');
			modules.push('/message/res/trumbowyg/plugins/colors/trumbowyg.colors.min.js');
			modules.push('/message/res/trumbowyg/plugins/pasteimage/trumbowyg.pasteimage.min.js');
			
			debugModules.push('/message/res/trumbowyg/trumbowyg.js');
			debugModules.push('/message/res/trumbowyg/langs/fr.min.js');
			debugModules.push('/message/res/trumbowyg/plugins/colors/trumbowyg.colors.min.js');
			debugModules.push('/message/res/trumbowyg/plugins/pasteimage/trumbowyg.pasteimage.min.js');
			
			butor.Loader.loadCSS('/message/res/trumbowyg/ui/trumbowyg.min.css');
			butor.Loader.loadCSS('/message/res/trumbowyg/plugins/colors/ui/trumbowyg.colors.min.css');
		}
		this.base('message', modules, debugModules);
		
		var self = this;

		this.onLoad(function() {
			App.addBundle('message', butor.message.bundles); 
			butor.message._message = new butor.message.MessageApp();
			
			butor.message._message.onReady(function() {
				App.translateElem($(".message-nav-bar"), 'message');
				App.translateElem($("#_content"), 'message');
				//self.ready();
			});
		});
	}
});

//# sourceURL=butor.message.boot.js
