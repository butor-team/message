/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.message.bundles = butor.message.bundles || {
	'New message' : {'fr':'Nouveau message'},
	'Inbox' : {'fr':'Boîte de réception'},
	'Sent messages' : {'fr':'Messages envoyés'},
	'Alert' : {'fr':'Alerte'},
	'Subject' : {'fr':'Objet'},
	'Recipients' : {'fr':'Destinataires'},
	'Validate' : {'fr':'Valider'},
	'Back' : {'fr':'Retour'},
	'Post' : {'fr':'Envoyer'},
	'Read' : {'fr':'Lire'},
	'Delete' : {'fr':'Supprimer'},
	'Compose a message' : {'fr':'Composer un message'},
	'View message' : {'fr':'Voir message'},

	'Group' : {'fr':'Groupe'},
	'Role' : {'fr':'Rôle'},
	'System' : {'fr':'Système'},
	'group' : {'fr':'Groupe', 'en':'Group'},
	'role' : {'fr':'Rôle', 'en':'Role'},
	'system' : {'fr':'Système', 'en':'System'},
	'firm' : {'fr':'Firme', 'en':'Firm'},
	'branch' : {'fr':'Succursale', 'en':'Branch'},

	'I.A.' : {'fr':'C.P.'},
	'I.A. lookup' : {'fr':'Recherche C.P.'},
	'THE END' : {'fr':'FIN'},
	
	'Drop files here': {'fr':'Glisser fichiers ici'},
	'Attachments': {'fr':'Pièces jointes'},
	'(no files attached)': {'fr':'(aucune pièce jointe)'}
};
