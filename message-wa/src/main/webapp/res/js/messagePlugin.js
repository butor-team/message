/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.message = butor.message || {
	// copy from boot
	'tr' : function(txt) {
		return App.tr(txt, 'message');
	},
	formatAttachSize : function(size) {
		var sizes = ['B', 'K','M','G','T'];
		if (size == 0) return '0 B';
		var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
		return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
	},
	fileToBase64 : function(file, handler) {
		var dfr =  $.Deferred();
		
		if (handler) {
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				handler && handler(reader.result);
				dfr.resolve();
			};
			reader.onerror = function (error) {
				console.log('Error: ', error);
				handler && handler(null);
				dfr.resolve();
			};
		}
		
		return dfr.promise();
	}
};

//copy from messageDlg
butor.message.MessageDlg = butor.message.MessageDlg || butor.dlg.Dialog.extend({
	statics : {
		showSentMsg : function(id) {
			if (!id)
				return;
			butor.message.MessageDlg.show({sentMsgId:id});
		},
		showMsg : function(id) {
			if (!id)
				return;
			butor.message.MessageDlg.show({id:id})
		},
		show : function(args) {
			dlgOpts = {
				title : butor.message.tr('Message'),
				bundleId:'message'
			};
			
			var dlg = new butor.message.MessageDlg(dlgOpts);
			dlg.show(args);
			return dlg;
		}
	},
	_jqe : null,
	tr : butor.message.tr,
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/message/messageDlg.html');
	},
	_init : function() {
		this.base();

		this._recipientFrom = this._jqe.find('#recipientFrom');
		this._recipientTo = this._jqe.find('#recipientTo');
		this._subject = this._jqe.find('#subject');
		this._message = this._jqe.find('#message');
		this._date = this._jqe.find('#date');
		this._attachZone = this._jqe.find('#attachments');
		this._attachmentTmpl = this._attachZone.find('.attachment-item').remove();

		this._closeBtn = this._jqe.find('#closeBtn').click($.proxy(function() {
			this.close();
		}, this));

		this._load();
	},
	_load : function() {
		this.hideMsg();
		this.mask(this.tr('Working'));
		
		var msgId = this._args['sentMsgId'] || this._args['id']
		
		this._reqId = butor.message.MessageAjax[this._args['sentMsgId'] ? 'readSentMessage' : 'readMessage'](msgId,
			$.proxy(function(resp) {
				this.unmask();
				if (!AJAX.isSuccessWithData(resp, this._reqId)) {
					return;
				}
				var message = resp['data'][0];

				this._subject.text(message['subject']);
				this._recipientFrom.text(message['fromUserId']);
				if (!this._args['sentMsgId']) {
					this._recipientTo.text(message['toUserId']);
				} else {
					var rcpts = $('<div>');
					
					var toRcpts = ($.parseJSON(message['toFilter']) || {})['recipients']||[];
					for (var i=0; i<toRcpts.length; i+=1) {
						var rcpt = butor.message._message._recipientTmpl.clone();
						// rcpt.find('.remove-message-recipient').remove();
						rcpt.find('.message-recipient-id').text(toRcpts[i]);
						rcpts.append(rcpt);
					}
					this._recipientTo.html(rcpts);
				}
				this._date.text(butor.Utils.formatDateTime(message['creationDate']));
				this._message.html(message['message']);
				
				var files = message['attachments']||[];
				if (files[0]) {
					this._attachZone.closest('.row').show();
					for (var i = 0; i < files.length; i++) {
						var f = files[i];
						var item = this._attachmentTmpl.clone();
						item.data('file', f);
						item.find('.attachment-name').text(f['fileName']);
						item.find('.attachment-size').text(butor.message.formatAttachSize(f['fileSize']));
						this._attachZone.append(item);
						item.show();
						var self = this;
						item.find('.download-attach-link').click(function(e) {
							var file = $(this).parent().data('file');
							self._download(file['fileId']);
							e.preventDefault();
							e.stopPropagation();
							return false;
						});
					}
				}
			}, this)
		);
	},
	_download : function(fileId) {
		this.hideMsg();
		this.mask(this.tr('Working'));
		if (this._args['sentMsgId'])
			butor.message.MessageAjax.downloadSentMessageAttach(this._args['sentMsgId'], fileId,
				$.proxy(function(resp) {
					this.unmask();
				}, this)
			);
		else
			butor.message.MessageAjax.downloadMessageAttach(this._args['id'], fileId,
				$.proxy(function(resp) {
					this.unmask();
				}, this)
			);
	}
});

// copy from mesage
butor.message.MessageAjax = butor.message.MessageAjax || function() {
	return {
		listMessage : function(criteria, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'listMessage'},
					[criteria], handler);
		},
		listSentMessage : function(criteria, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'listSentMessage'},
					[criteria], handler);
		},
		listBranch : function(handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'listBranch'},
					[], handler);
		},
		readMessage : function(id, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'readMessage'},
					[id], handler);
		},
		downloadMessageAttach : function(id, fileId, handler) {
			return AJAX.call('/message/message.bin.ajax',
					{'download':true,
					'service': 'downloadMessageAttach'},
					[id, fileId], handler);
		},
		downloadSentMessageAttach : function(id, fileId, handler) {
			return AJAX.call('/message/message.bin.ajax',
					{'download':true,
					'service': 'downloadSentMessageAttach'},
					[id, fileId], handler);
		},
		readSentMessage : function(id, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'readSentMessage'},
					[id], handler);
		},
		validateRecipients : function(criteria, handler) {
			return AJAX.call('/message/message.ajax', 
				{'streaming':false, 
				'service':'validateRecipients'},
				[criteria], handler);
		},
		sendMessage : function(criteria, handler) {
			return AJAX.call('/message/message.ajax', 
				{'streaming':false, 
				'service':'sendMessage'},
				[criteria], handler);
		},
		deleteMessage : function(mkl, handler) {
			return AJAX.call('/message/message.ajax',
				{'streaming':false,
				'service':'deleteMessage'},
				[mkl], handler);
		},
		markMessageRead : function(mkl, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming':false,
					'service':'markMessageRead'},
					[mkl], handler);
		}
	};
}();

new function() {
	if (!App.hasAccess('message','message')) {
		return;
	}
	
	App.Bundle.override('common', {
		'more alerts!': {'fr' : 'alertes supplementaires!'},
		'View message' : {'fr':'Voir message'},
		'Subject' : {'fr':'Objet'}
	});
	
	// add message indicator
	var rm = $('#_header ul.navbar-right');
	var messageLink = $('<li><a id="messageLink">' +
			'<i class="fa fa-envelope text-primary"></i><span class="message-count bolder" style="padding:0px 3px 0px 3px;margin-top:5px;margin-left:-2px;font-size:90%;"></span></a></li>');
	messageLink.insertBefore('#userMenuItem');
	messageLink.find('a').click(function(){
		App.showPage('message.message');
		checkMessage();
	});
	
	var messageIcon = messageLink.find('.fa');
	var messageCount = messageLink.find('.message-count');
//	var timer = new butor.Timer(function() {
//		messageIcon.fadeOut(200).fadeIn(100).fadeOut(100).fadeIn(200);
//	}, 3000);

	function checkMessage() {
		var reqId = AJAX.call('/message/message.ajax', {'streaming': false,
			'service': 'checkMessage'},
			[{}], function(resp) {
				if (!AJAX.isSuccess(resp, reqId)) {
					return;
				}
				var count = resp['data'];
				if (count>0) {
					messageIcon.css('color', '#FF8652');
					messageCount.html(count);
//					timer.start();
				} else {
					messageIcon.css('color', '');
					messageCount.html('');
//					timer.stop();
				}
			}
		);
	}
	var maxAlerts = 5;
	var lRequId;
	var checkAlerts = function() {
		var crit = {'msgType':'alert', 'status':0};
		lRequId = AJAX.call('/message/message.ajax', {'streaming': false,
			'service': 'listMessage'}, [crit], function(resp) {
				if (!AJAX.isSuccessWithData(resp, lRequId)) {
					return;
				}
				
				var vml = {};
				var vCount = 0;
				var al = App.getBottomPanel().find('.alert-message').each(function(){
					var elm = $(this);
					vml[''+elm.attr('msgId')] = true;
					vCount += 1;
				});
				
				var data = resp['data']||[];

				var now = new Date();
				//if (vCount < maxAlerts) {
					for (var ii=0; ii<data.length && ii<maxAlerts; ii++) {
						var message = data[ii];
						if (message['endDate'] && butor.Utils.parseDate(message['endDate']) < now) {
							continue;
						}
						if (vml[message['id']] != null) {
							vml[message['id']] = false;
							App.getBottomPanel().remove(App.getBottomPanel().find('[msgId="' +message['id'] +'"]'));
							//continue;
						}
						showAlert(message);
					}
				//}
				
				vml['moreMessages'] = false;
				App.getBottomPanel().remove(App.getBottomPanel().find('[msgId="moreMessages"]'));

				var rem = data.length-maxAlerts;
				if (rem > 0) {
					var msg = {'id':'moreMessages',
						'message': '<a href="#t=p&p=message.message&' +butor.Utils.getUUID() +'=">' +rem +' <span class="bundle">more alerts!</span></a>'};
					showAlert(msg);
					App.translateElem(App.getBottomPanel().find('[msgId="' +msg.id +'"]'));
				}

				for (var msgId in vml) {
					if (vml[msgId] == true) {
						var elm = App.getBottomPanel().find('[msgId="' +msgId +'"]');
						App.getBottomPanel().remove(elm);
					}
				}

			}
		);
	}
	
	var rReqId;
	var showAlert = function(message) {
		var al = '<div class="alert-message" msgId="' +message['id'] +
			'" style="border-bottom:1px solid gray;">';
		
		if (message['id'] == 'moreMessages') {
			al += '<i class="fa fa-warning" style="color:red;"></i> ' +message['message'];
		} else {
			if (message['msgType'] == 'alert') {
				al += '<i class="fa fa-warning" style="color:red;"></i> ';
			}
	
			if (message['creationDate']) {
				al += ' <span class="pull-right">' +butor.Utils.formatDateTime(message['creationDate']) +'</span>';
			}
	
	
			al += 'Message ' +butor.message.tr('From') +': <span class="bolder">' +(message['fromUserDisplayName']||message['fromUserId']) +'</span>';
	
			if (message['subject']) {
				al += ', ' +butor.message.tr('Subject') +': <span class="bolder">' +message['subject'] +'</span>';
			}
			
			if (message['nbAttach']) {
				al += '<span class="has-attachments" style="margin-left:20px;"><i class="fa fa-paperclip"></i></span>';
			}
	
			al += '<button style="margin-left:30px;" class="btn btn-primary btn-xs btn-default btn-close-alert-message" style="">' +butor.message.tr('View message') +'</button>';
	
			al += '</div>';
	
			// al += message['message'];
		}

		al += '</div></div>';
		
		al = $(al);
		
		al.find('.btn-close-alert-message').click(function() {
			if (message['id'] == 'moreMessages') {
				App.getBottomPanel().remove(al);
			} else {
				butor.message.MessageDlg.showMsg(message['id']);
				App.getBottomPanel().remove(al);
			}
		});
		App.getBottomPanel().add(al);
	};
	
	var notifHandler = function(e) {
		var notif = e.data;
		//delay to give time to the the backend process to persist data.
		setTimeout(function() {
			refreshMessages();
			App.fire(notif.type, notif.data);
		}, 1000);
	};
	
	App.notif.subscribe('message-received', notifHandler);
	App.notif.subscribe('message-read', notifHandler);
	App.notif.subscribe('message-deleted', notifHandler);

	App.bind('message-read', function(e) {
		ml = e.data;
		refreshMessages();
	});

	var checkTimer = null;
	var refreshMessages = function() {
		if (checkTimer != null) {
			clearTimeout(checkTimer);
		}
		checkMessage();
		checkAlerts();
		
		checkTimer = setTimeout(refreshMessages, 3000 *60);
	};

	refreshMessages();
}();

//# sourceURL=butor.message.messagePlugin.js
