/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.message.Sent = butor.message.Sent || butor.Class.define({
	_selectedItemFields : ['id'],
	_reqId : null,
	_reqId2 : null,
	_cReqId : null,
	_ctxMenu : null,
	_firms : {},
	_jqe : null,
	_tableCtxtMenu : null,
	_canSend : App.hasAccess('message', 'sendMessage'),
	construct : function(jqe) {
		this._jqe = jqe;
		this._clientName = this._jqe.find('#clientName');
		this._statusSB = this._jqe.find('#statusSB');
		this._repId = this._jqe.find('#repId');
		this._typeSB = this._jqe.find('#typeSB');
		this._table = this._jqe.find('#resultTable');
		this._tbody = this._table.find('tbody'); 
		this._rowTmpl = this._tbody.find('tr').remove(); 
		this._table.tablesorter({textExtraction : 
			function(node) {
				var val = node.textContent;
				if (val == '') {
					val = node.innerHTML;
				}
				return val;
			}
		});

		this._pageRemovedP = $.proxy(function(e) {
			if (e.data == 'message.message') {
				this._cleanup();
			}
		},this);
		App.bind('removing-page', this._pageRemovedP);

		this._listP = $.proxy(this._list, this);
		this._messageReadP = $.proxy(function(e) {
			if (e.data.ml) {
				for (var i=0;i<e.data.ml.length; i++)
					this._messageRead(e.data.ml[i]);
			}
		}, this);

		this._ctxMenu = this._jqe.find('#ctxMenu');
		this._ctxMenu.find('li.view a').click($.proxy(function(e_) {
			var row = this._tableCtxtMenu.getHoveredRow();
			this._openMessage(row);
		}, this));
		if (this._canSend) {
			this._ctxMenu.find('li.create-new').show().find('a').click($.proxy(function(e_) {
				var row = this._tableCtxtMenu.getHoveredRow();
				this._createNewMessage(row);
			}, this));
		}
		this._tableCtxtMenu = new butor.TableCtxtMenu(this._table, this._ctxMenu);
		this._tableCtxtMenu.setSelectionFields(this._selectedItemFields);
		this._tableCtxtMenu.bind('row-hovered', $.proxy(function(e) {
			this._ctxMenu.find('li.view').show();
			if (this._canSend) {
				//this._ctxMenu.find('li.delete').show();
				this._ctxMenu.find('li.create-new').show();
			} else {
				//this._ctxMenu.find('li.delete').hide();
				this._ctxMenu.find('li.create-new').hide();
			}
		}, this));

		App.bind('message-read', this._messageReadP);
		App.bind('message-received', this._listP);
		
		this._jqe.find('#goBtn').click(this._listP);
		
		this._tbody.delegate('td.message-message .message-subject', 'click', $.proxy(function() {
			var row = this._tableCtxtMenu.getHoveredRow();
			this._openMessage(row);
			return false;
		}, this));

		this._jqe.find('a.message-select-all').click($.proxy(function(){this._selectAll();return false;},this));
		this._jqe.find('a.message-select-none').click($.proxy(function(){this._selectNone();return false;},this));
		butor.message._message.onReady($.proxy(function() {
			butor.Utils.fillSelect(this._statusSB, butor.message._message.getAttrSet('clientStatus'), {'showEmpty':true});
			this.reset();
			this._list();
		}, this));
	},
	reset : function() {
		this._clientName.val('');

		//TODO
		//this._statusSB.val('PENDING');
		this._statusSB.val('');
		this._typeSB.val('');
		this.clear();
	},
	clear : function() {
		if (this._ctxMenu) {
			this._ctxMenu.hide();
		}
		this._tbody.empty();
	},
	_cleanup : function() {
		App.unbind('removing-page', this._pageRemovedP);
		App.unbind('message-read', this._messageReadP);
		App.unbind('message-received', this._listP);
	},
	_selectAll : function() {
		this._table.find('.message-select input').attr('checked', true);
	},
	_selectNone : function() {
		this._table.find('.message-select input').attr('checked', false);
	},
	_list : function() {
		App.hideMsg();
		App.mask(this.tr('Working'));
		this.clear();
		var crit = {};
		this._cReqId = butor.message.MessageAjax.listSentMessage(crit,
			$.proxy(function(resp) {
				App.unmask();
				if (!AJAX.isSuccessWithData(resp, this._cReqId)) {
					return;
				}
				var data = resp['data'];
				for (var ii=0; ii<data.length; ii++) {
					var message = data[ii];
					var row = this._rowTmpl.clone();
					this._showRowInfo(row, message);
					this._tbody.append(row);
					row.show();
				}
				this._table.trigger("update");
			}, this)
		);
	},
	_showRowInfo : function(row, message) {
		var msg = $('<div>');
		msg.append(this._formatSubject(message));
		msg.append(this._formatTo(message));
		row.find('.message-message').html(msg);
		row.find('.creationDate').html(butor.Utils.formatDateTime(message['creationDate']));
		row.attr('id', message['id']);
		row.data('data', message);
	},
	_messageRead : function(messageKey) {
		if (!messageKey || !messageKey.id) {
			return;
		}
		
		var row = this._table.find('tr[id=' +messageKey.id +']');
		if (!row)
			return;

		var message = row.data('data');
		if (!message)
			return;
		message['status'] = 1;
		this._showRowInfo(row, message);
	},
	_formatTo : function(message) {
		var to = $('<div class="message-to-filter">');
		to.append(this.tr('To') +' : ');
		var toFilters = ($.parseJSON(message['toFilter']) || {})['recipientFilterList']||[];
		for (var i=0; i<toFilters.length; i+=1) {
			var item = toFilters[i];
			var rcpt = butor.message._message._recipientTmpl.clone();
			rcpt.find('.remove-message-recipient').remove();
			to.append(rcpt);
			
			if (item['type'] == 'USER') {
				rcpt.find('.message-recipient-id').text(this.tr('User') +' : ' +item['id']);
			} else {
				var type = item['type'].toLowerCase();
				rcpt.find('.message-recipient-id').text(this.tr(type) +' : ' +butor.message._message.getRcptTypeHelper(type).get(item['id']) || item['id']);
			}
		}
		return to;
	},
	_formatSubject : function(message) {
		var icon = '<i class="fa';
		if (message['msgType'] == 'alert') {
			icon += ' fa-warning"';
			if (message['status'] == 0) {
				icon += ' style="color:red;"';
			}
		} else {
			icon += ' fa-info-circle"';
			if (message['status'] == 0) {
				icon += ' style="color:blue;"';
			}
		}
		icon += '></i>';

		var subject = icon +' ' +message['subject']; 
		
		if (message['nbAttach']) {
			subject += '<span class="has-attachments" style="margin-left:20px;"><i class="fa fa-paperclip"></i></span>';
		}

		return '<div><a href="#" class="message-subject">' +subject +'</a></div>';
	},
	_openMessage : function(row) {
		var message = row.data('data');
		butor.message.MessageDlg.showSentMsg(message['id']);
	},
	_createNewMessage : function(row) {
		var message = row.data('data');
		if (message['message'] == null) {
			App.mask(this.tr('Working'));
			var rreqId = butor.message.MessageAjax.readSentMessage(message['id'],
				$.proxy(function(resp) {
					App.unmask();
					if (!AJAX.isSuccessWithData(resp, rreqId)) {
						return;
					}
					var message = resp['data'][0];
					row.data('data', message);
					var dlg = butor.message.NewMessageDlg.show(message);
				}, this)
			);
		} else {
			var dlg = butor.message.NewMessageDlg.show(message);
		}
	}
});

//# sourceURL=butor.message.sent.js
