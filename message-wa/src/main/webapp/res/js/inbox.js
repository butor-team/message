/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.message.Inbox = butor.message.Inbox || butor.Class.define({
	_selectedItemFields : ['id'],
	_reqId : null,
	_reqId2 : null,
	_cReqId : null,
	_ctxMenu : null,
	_firms : {},
	_jqe : null,
	_tableCtxtMenu : null,
	_canUpdate : App.hasAccess('message', 'message', butor.sec.ACCESS_MODE_WRITE),
	construct : function(jqe) {
		this._jqe = jqe;
		this._clientName = this._jqe.find('#clientName');
		this._statusSB = this._jqe.find('#statusSB');
		this._repId = this._jqe.find('#repId');
		this._typeSB = this._jqe.find('#typeSB');
		this._table = this._jqe.find('#resultTable');
		this._tbody = this._table.find('tbody'); 
		this._rowTmpl = this._tbody.find('tr').remove(); 
		this._table.tablesorter({textExtraction : 
			function(node) {
				var val = node.textContent;
				if (val == '') {
					val = node.innerHTML;
				}
				return val;
			}
		});

		this._pageRemovedP = $.proxy(function(e) {
			if (e.data == 'message.message') {
				this._cleanup();
			}
		},this);
		App.bind('removing-page', this._pageRemovedP);

		this._listP = $.proxy(this._list, this);
		this._messageReadP = $.proxy(function(e) {
			if (e.data.ml) {
				for (var i=0;i<e.data.ml.length; i++)
					this._messageRead(e.data.ml[i]);
			}
		}, this);

		this._ctxMenu = this._jqe.find('#ctxMenu');
		this._ctxMenu.find('li.view a').click($.proxy(function(e_) {
			this._readSelectedMessage();
		}, this));
		if (this._canUpdate) {
			this._ctxMenu.find('li.delete').show().find('a').click($.proxy(function(e_) {
				this._deleteSelectedMessage();
			}, this));
		}
		this._tableCtxtMenu = new butor.TableCtxtMenu(this._table, this._ctxMenu);
		this._tableCtxtMenu.setSelectionFields(this._selectedItemFields);
		this._tableCtxtMenu.bind('row-hovered', $.proxy(function(e) {
			this._ctxMenu.find('li.view').show();
			if (this._canUpdate) {
				this._ctxMenu.find('li.delete').show();
			} else {
				this._ctxMenu.find('li.delete').hide();
			}
		}, this));

		App.bind('message-read', this._messageReadP);
		App.bind('message-received', this._listP);
		
		this._jqe.find('#goBtn').click(this._listP);
		
		this._tbody.delegate('td.message-message .message-subject', 'click', $.proxy(function() {
			var row = this._tableCtxtMenu.getHoveredRow();
			if (row.data('opened')) {
				this._closeMessage(row);
			} else {
				this._readMessage(row);
			}
			return false;
		}, this));

		this._jqe.find('a.message-select-all').click($.proxy(function(){this._selectAll();return false;},this));
		this._jqe.find('a.message-select-none').click($.proxy(function(){this._selectNone();return false;},this));
		butor.message._message.onReady($.proxy(function() {
			butor.Utils.fillSelect(this._statusSB, butor.message._message.getAttrSet('clientStatus'), {'showEmpty':true});
			this.reset();
			this._list();
		}, this));
	},
	reset : function() {
		this._clientName.val('');

		//TODO
		//this._statusSB.val('PENDING');
		this._statusSB.val('');
		this._typeSB.val('');
		this.clear();
	},
	clear : function() {
		if (this._ctxMenu) {
			this._ctxMenu.hide();
		}
		this._tbody.empty();
	},
	_cleanup : function() {
		App.unbind('removing-page', this._pageRemovedP);
		App.unbind('message-read', this._messageReadP);
		App.unbind('message-received', this._listP);
	},
	_selectAll : function() {
		this._table.find('.message-select input').attr('checked', true);
	},
	_selectNone : function() {
		this._table.find('.message-select input').attr('checked', false);
	},
	_list : function() {
		App.hideMsg();
		App.mask(this.tr('Working'));
		this.clear();
		var crit = {};
		this._cReqId = butor.message.MessageAjax.listMessage(crit,
			$.proxy(function(resp) {
				App.unmask();
				if (!AJAX.isSuccessWithData(resp, this._cReqId)) {
					return;
				}
				var data = resp['data'];
				for (var ii=0; ii<data.length; ii++) {
					var message = data[ii];
					var row = this._rowTmpl.clone();
					this._showRowInfo(row, message);
					this._tbody.append(row);
					row.show();
				}
				this._table.trigger("update");
			}, this)
		);
	},
	_showRowInfo : function(row, message) {
		row.find('.message-message').html(this._formatMessage(message));
		row.find('.fromUserId').text(message['fromUserDisplayName']||message['fromUserId']);
		row.find('.creationDate').text(butor.Utils.formatDateTime(message['creationDate']));
		row.attr('id', message['id']);
		row.data('data', message);
	},
	_messageRead : function(messageKey) {
		if (!messageKey || !messageKey.id) {
			return;
		}
		
		var row = this._table.find('tr[id=' +messageKey.id +']');
		if (!row) {
			return;
		}

		if (row.data('opened')) {
			return;
		}
		
		var message = row.data('data');
		message['status'] = 1;
		this._showRowInfo(row, message);
	},
	_formatMessage : function(message) {
		var icon = '<i class="fa';
		if (message['msgType'] == 'alert') {
			icon += ' fa-warning"';
			if (message['status'] == 0) {
				icon += ' style="color:red;"';
			}
		} else {
			icon += ' fa-info-circle"';
			if (message['status'] == 0) {
				icon += ' style="color:blue;"';
			}
		}
		icon += '></i>';

		var subject = icon +' ' +message['subject']; 
		if (message['status'] == 0) {
			subject = '<span class="bolder">' +subject +'</span>';
		}
		
		if (message['nbAttach']) {
			subject += '<span class="has-attachments" style="margin-left:20px;"><i class="fa fa-paperclip"></i></span>';
		}
		return '<a href="#" class="message-subject">' +subject +'</a>';
	},
	_getSelectedRows : function() {
		var sel = [];
		this._table.find('.message-select input:checked').each(function(){
			var row = $(this).closest('tr');
			sel.push(row);
		});
		if (sel.length == 0) {
			sel.push(this._tableCtxtMenu.getHoveredRow())
		}
		return sel;
	},
	_openMessage : function(row) {
		var message = row.data('data');
		butor.message.MessageDlg.showMsg(message['id']);
	},
	_closeMessage : function(row) {
		var message = row.data('data');
		var td = row.find('td.message-message');
		td.html(this._formatMessage(message));
		row.data('opened', false);
	},
	_readMessage : function(row) {
		var message = row.data('data');
		if (message['message'] == null) {
			App.mask(this.tr('Working'));
			var rreqId = butor.message.MessageAjax.readMessage(message['id'],
				$.proxy(function(resp) {
					App.unmask();
					if (!AJAX.isSuccessWithData(resp, rreqId)) {
						return;
					}
					var message = resp['data'][0];
					row.data('data', message);
					this._openMessage(row);
				}, this)
			);
		} else {
			this._openMessage(row);
		}

		//this._markMessageRead([{'id':message['id'], 'revNo':message['revNo']}]);
	},
	_markMessageRead : function(ml) {
		var args = {'keyList':ml}
		var reqId = butor.message.MessageAjax.markMessageRead(args,
			$.proxy(function(resp) {
				App.unmask();
				if (!AJAX.isSuccess(resp, reqId)) {
					return;
				}
				App.fire('message-read', {ml:ml});
			}, this)
		);
	},
	_readSelectedMessage : function() {
		var rows = this._getSelectedRows();
		var ml = [];
		for (var i=0;i<rows.length;i+=1) {
			var message = rows[i].data('data');
			this._openMessage(rows[i]);
			ml.push({'id':message['id'], 'revNo':message['revNo']});
		}
		this._markMessageRead(ml);
	},
	_deleteSelectedMessage : function() {
		var rows = this._getSelectedRows();
		var ml = [];
		for (var i=0;i<rows.length;i+=1) {
			var message = rows[i].data('data');
			ml.push({'id':message['id'], 'revNo':message['revNo']});
		}
		this._deleteMessage(ml);
	},
	_deleteMessage : function(ml) {
		App.mask(this.tr('Working'));
		var args = {'keyList':ml}
		var reqId = butor.message.MessageAjax.deleteMessage(args,
			$.proxy(function(resp) {
				App.unmask();
				if (!AJAX.isSuccess(resp, reqId)) {
					return;
				}
				this._list();
			}, this)
		);
	}
});

//# sourceURL=butor.message.inbox.js
