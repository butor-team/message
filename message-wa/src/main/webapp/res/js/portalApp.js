/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//TODO move to fw
butor.portal = butor.portal || {};
butor.portal.PortalApp = butor.portal.PortalApp || butor.Class.define({
	_attrSet : {},
	_attrSetHelper : {},
	_attrSetsToLoad : [],
	_ready : false,
	_ctx : null,
	_formServices : null,
	construct : function(ctx, attrSetsToLoad) {
		//this.base();
		this._ctx = ctx;
		this._attrSetsToLoad = attrSetsToLoad;
		var self = this;		
		this._langChangedP = $.proxy(this._langChanged, this);
		App.bind('langChanged', this._langChangedP);
		this._pageLoadedP = $.proxy(this._pageLoaded, this);

		this._navbarRemovedP = $.proxy(function(e) {
			if (e.data == this.getCtx()) {
				this._cleanup();
			}
		}, this);
		App.bind('removing-navbar', this._navbarRemovedP);

		//show hide user-search-section depending on selected page
		App.bind('page-loaded', this._pageLoadedP);

		$.when(
			this._loadCodeSets(),
			this._loadMoreSettings())
		.always($.proxy(function() {
			this._ready = true;
			this.fire('ready', null);
			this._init();
		}, this));
	},
	tr : function(text) {
		return App.tr(text, this._ctx);
	},
	getCtx : function() {
		return this._ctx;
	},
	_pageLoaded : function(e) {
		
	},
	_cleanup : function() {
		App.unbind('langChanged', this._langChangedP);
		App.unbind('removing-navbar', this._navbarRemovedP);
		App.unbind('page-loaded', this._pageLoadedP);
	},
	_langChanged : function() {
		//TODO
	},
	onReady : function(callback) {
		this.bind('ready', callback);
		if (this._ready) {
			callback();
			this.unbind('ready', callback);
		}
	},
	_loadCodeSets : function() {
		var dfr =  $.Deferred();
		if (!this._attrSetsToLoad || this._attrSetsToLoad.length == 0) {
			dfr.resolve();
			return dfr.promise();
		}
		var reqId1 = App.AttrSet.getCodeSets(this._attrSetsToLoad, {'scope':this, 'callback':
			function(result) {
				if (!AJAX.isSuccessWithData(result, reqId1)) {
					dfr.reject();
					return;
				}
				for (var i=0; i<this._attrSetsToLoad.length; i+=1) {
					var setId = this._attrSetsToLoad[i]['id'];
					this._attrSet[setId] = result.data[i];
					this._attrSetHelper[setId] = App.AttrSet.createHelper(result.data[i]);
				}

				dfr.resolve();
				this.fire('codeSet-loaded', null);
			}});
		return dfr.promise();
	},
	_loadMoreSettings : function() {
		// override
		var dfr =  $.Deferred();
		dfr.resolve();
		return dfr.promise();
	},
	_init : function() {
		// override
		// app is ready
	},
	getAttrSet : function(id) {
		return this._attrSet[id];
	},
	getAttrSetHelper : function(id) {
		return this._attrSetHelper[id];
	}
});

//# sourceURL=butor.portal.portalApp.js
