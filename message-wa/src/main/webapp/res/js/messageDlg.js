/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.message.MessageDlg = butor.message.MessageDlg || butor.dlg.Dialog.extend({
	statics : {
		showSentMsg : function(id) {
			if (!id)
				return;
			butor.message.MessageDlg.show({sentMsgId:id});
		},
		showMsg : function(id) {
			if (!id)
				return;
			butor.message.MessageDlg.show({id:id})
		},
		show : function(args) {
			dlgOpts = {
				title : butor.message.tr('Message'),
				bundleId:'message'
			};
			
			var dlg = new butor.message.MessageDlg(dlgOpts);
			dlg.show(args);
			return dlg;
		}
	},
	_jqe : null,
	tr : butor.message.tr,
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/message/messageDlg.html');
	},
	_init : function() {
		this.base();

		this._recipientFrom = this._jqe.find('#recipientFrom');
		this._recipientTo = this._jqe.find('#recipientTo');
		this._subject = this._jqe.find('#subject');
		this._message = this._jqe.find('#message');
		this._date = this._jqe.find('#date');
		this._attachZone = this._jqe.find('#attachments');
		this._attachmentTmpl = this._attachZone.find('.attachment-item').remove();

		this._closeBtn = this._jqe.find('#closeBtn').click($.proxy(function() {
			this.close();
		}, this));

		this._load();
	},
	_load : function() {
		this.hideMsg();
		this.mask(this.tr('Working'));
		
		var msgId = this._args['sentMsgId'] || this._args['id']
		
		this._reqId = butor.message.MessageAjax[this._args['sentMsgId'] ? 'readSentMessage' : 'readMessage'](msgId,
			$.proxy(function(resp) {
				this.unmask();
				if (!AJAX.isSuccessWithData(resp, this._reqId)) {
					return;
				}
				var message = resp['data'][0];

				this._subject.text(message['subject']);
				this._recipientFrom.text(message['fromUserId']);
				if (!this._args['sentMsgId']) {
					this._recipientTo.text(message['toUserId']);
				} else {
					var rcpts = $('<div>');
					
					var toRcpts = ($.parseJSON(message['toFilter']) || {})['recipients']||[];
					for (var i=0; i<toRcpts.length; i+=1) {
						var rcpt = butor.message._message._recipientTmpl.clone();
						// rcpt.find('.remove-message-recipient').remove();
						rcpt.find('.message-recipient-id').text(toRcpts[i]);
						rcpts.append(rcpt);
					}
					this._recipientTo.html(rcpts);
				}
				this._date.text(butor.Utils.formatDateTime(message['creationDate']));
				this._message.html(message['message']);
				
				var files = message['attachments']||[];
				if (files[0]) {
					this._attachZone.closest('.row').show();
					for (var i = 0; i < files.length; i++) {
						var f = files[i];
						var item = this._attachmentTmpl.clone();
						item.data('file', f);
						item.find('.attachment-name').text(f['fileName']);
						item.find('.attachment-size').text(butor.message.formatAttachSize(f['fileSize']));
						this._attachZone.append(item);
						item.show();
						var self = this;
						item.find('.download-attach-link').click(function(e) {
							var file = $(this).parent().data('file');
							self._download(file['fileId']);
							e.preventDefault();
							e.stopPropagation();
							return false;
						});
					}
				}
			}, this)
		);
	},
	_download : function(fileId) {
		this.hideMsg();
		this.mask(this.tr('Working'));
		if (this._args['sentMsgId'])
			butor.message.MessageAjax.downloadSentMessageAttach(this._args['sentMsgId'], fileId,
				$.proxy(function(resp) {
					this.unmask();
				}, this)
			);
		else
			butor.message.MessageAjax.downloadMessageAttach(this._args['id'], fileId,
				$.proxy(function(resp) {
					this.unmask();
				}, this)
			);
	}
});

//# sourceURL=butor.message.messageDlg.js
