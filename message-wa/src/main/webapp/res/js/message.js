/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
butor.message.MessageApp = butor.message.MessageApp || butor.portal.PortalApp.define({
	//TODO which attrset?
	//{'sysId':"message", 'id':"y"}
	_attrSetsToLoad : [
	],
	_navBar : null,
	_inboxLink : null,
	_sentLink : null,
	_refreshLink : null,
	_typeHelper : {},
	construct : function() {
		this.base('message', this._attrSetsToLoad);
		
		this._appRemoved = $.proxy(function(e){
			if (e.data == 'message') {
				App.unbind('removing-navbar', this._appRemoved);
			}
		},this);
		App.bind('removing-navbar', this._appRemoved);
	},
	getRcptTypeHelper : function(type) {
		return this._typeHelper[type];
	},
	_langChanged : function() {
		//TODO
	},
	_loadMoreSettings : function() {
		// nothing to load more
		return this.base();
	},
	_init : function() {
		this.base();
		this._navBar = $(".message-nav-bar");
		this._inboxLink = this._navBar.find("#inboxLink");
		this._sentLink = this._navBar.find("#sentLink");
		this._sendMessageLink = this._navBar.find("#sendMessageLink");
		this._refreshLink = this._navBar.find('#refreshLink');

		this._recipientTmpl = this._navBar.find('div.message-recipient').remove().removeAttr('style');

		if (App.hasAccess('message','message')) {
			this._inboxLink.show().click(function(e_) {
				App.showPage('message.message');
			});
		}
		if (App.hasAccess('message','sendMessage', butor.sec.ACCESS_MODE_WRITE)) {
			this._sentLink.show().click(function(e_) {
				App.showPage('message.sentMessage');
			});
			this._sendMessageLink.show().click(function(e_) {
				//App.showPage('message.sendMessage');
				var dlg = butor.message.NewMessageDlg.show();
				return false;
			});
		}

		this._refreshLink.click($.proxy(function() {
			App.fire('message-received')
		}, this));

//		this._navBar.find("#messageLookupLink").show();
//		var cl = new butor.message.MessageLookup(this._navBar.find("#lookupLink input"), 
//				App.getMsgPanel(), 'message');
//		cl.bind('client-selected', function(e) {
//			var clientId = e.data;
//			if (clientId) {
//				butor.form.message.updateApplication(clientId);
//				cl.clear();
//			}
//		});

		$.when(
			this._loadFirms(),
			this._loadBranches(),
			this._loadSystems(),
			this._loadRoles(),
			this._loadGroups())
		.always($.proxy(function() {
			butor.message._loader.ready();
		}, this));
	},
	_pageLoaded : function(e) {
		if (e.data == 'message.message') {
			this._refreshLink.show();
		} else {
			this._refreshLink.hide();
		}
	},
	_loadFirms : function() {
		var dfr =  $.Deferred();
		this._fReqId = butor.sec.firm.FirmAjax.listFirm({sys:"sec", func:"firms", mode:"READ"}, 
			$.proxy(function(result) {
				if (!AJAX.isSuccess(result, this._fReqId)) {
					dfr.reject();
					return;
				}
				var data = result.data || [];
				for (var ii=0; ii<data.length; ii++) {
					var firm = data[ii];
					firm.k1 = firm.firmId;
					firm.value = firm.firmName;
				}
				this._typeHelper['firm'] = App.AttrSet.createHelper(data);
				dfr.resolve();
			}, this)
		);
		return dfr.promise();
	},
	_loadBranches : function() {
		var dfr =  $.Deferred();
		this._brReqId = butor.message.MessageAjax.listBranch(
			$.proxy(function(result) {
				if (!AJAX.isSuccess(result, this._brReqId)) {
					dfr.reject();
					return;
				}
				var data = result.data||[];
				for (var ii=0; ii<data.length; ii++) {
					var br = data[ii];
					br.k1 = br.branchCode;
					br.value = br.branchName;
				}
				this._typeHelper['branch'] = App.AttrSet.createHelper(data);
				dfr.resolve();
			}, this)
		);
		return dfr.promise();
	},
	_loadSystems : function() {
		var dfr =  $.Deferred();
		this._sReqId = butor.sec.auth.AuthAjax.listAuthSys('sec', 'auths',
			$.proxy(function(result) {
				if (!AJAX.isSuccessWithData(result, this._sReqId)) {
					dfr.reject();
					return;
				}
				this._typeHelper['system'] = App.AttrSet.createHelper(result.data);
				dfr.resolve();
			}, this));
		return dfr.promise();
	},
	_loadRoles : function() {
		var dfr =  $.Deferred();
		this._rReqId = butor.sec.role.RoleAjax.listRole(null, "auths", $.proxy(function(result) {
			if (!AJAX.isSuccess(result, this._rReqId)) {
				dfr.reject();
				return;
			}
			var data = result.data || [];
			for (var ii=0; ii<data.length; ii++) {
				var role = data[ii];
				role.k1 = role.id;
				role.value = role.description;
			}
			this._typeHelper['role'] = App.AttrSet.createHelper(data);
			dfr.resolve();
		}, this));
		return dfr.promise();
	},
	_loadGroups : function() {
		var dfr =  $.Deferred();
		this._gReqId = butor.sec.group.GroupAjax.listGroup(null, "auths", $.proxy(function(result) {
			if (!AJAX.isSuccess(result, this._gReqId)) {
				dfr.reject();
				return;
			}
			var data = result.data || [];
			for (var ii=0; ii<data.length; ii++) {
				var grp = data[ii];
				grp.k1 = grp.id;
				grp.value = grp.description;
			}
			this._typeHelper['group'] = App.AttrSet.createHelper(data);
			dfr.resolve();
		}, this));
		return dfr.promise();
	}
});

butor.message.MessageAjax = butor.message.MessageAjax || function() {
	return {
		listMessage : function(criteria, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'listMessage'},
					[criteria], handler);
		},
		listSentMessage : function(criteria, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'listSentMessage'},
					[criteria], handler);
		},
		listBranch : function(handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'listBranch'},
					[], handler);
		},
		readMessage : function(id, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'readMessage'},
					[id], handler);
		},
		downloadMessageAttach : function(id, fileId, handler) {
			return AJAX.call('/message/message.bin.ajax',
					{'download':true,
					'service': 'downloadMessageAttach'},
					[id, fileId], handler);
		},
		downloadSentMessageAttach : function(id, fileId, handler) {
			return AJAX.call('/message/message.bin.ajax',
					{'download':true,
					'service': 'downloadSentMessageAttach'},
					[id, fileId], handler);
		},
		readSentMessage : function(id, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming': false,
					'service': 'readSentMessage'},
					[id], handler);
		},
		validateRecipients : function(criteria, handler) {
			return AJAX.call('/message/message.ajax', 
				{'streaming':false, 
				'service':'validateRecipients'},
				[criteria], handler);
		},
		sendMessage : function(criteria, handler) {
			return AJAX.call('/message/message.ajax', 
				{'streaming':false, 
				'service':'sendMessage'},
				[criteria], handler);
		},
		deleteMessage : function(mkl, handler) {
			return AJAX.call('/message/message.ajax',
				{'streaming':false,
				'service':'deleteMessage'},
				[mkl], handler);
		},
		markMessageRead : function(mkl, handler) {
			return AJAX.call('/message/message.ajax',
					{'streaming':false,
					'service':'markMessageRead'},
					[mkl], handler);
		}
	};
}();

butor.message.NewMessageDlg = butor.message.NewMessageDlg || butor.dlg.Dialog.extend({
	statics : {
		show : function(args) {
			args = args||{};
			dlgOpts = {
				title : butor.message.tr('Message'),
				bundleId:'message'
			};

			var dlg = new butor.message.NewMessageDlg(dlgOpts);
			dlg.show(args);
			return dlg;
		}
	},
	_reqId : null,
	_jqe : null,
	_canUpdate : true, //TODO App.hasAccess('sec', 'users', butor.sec.ACCESS_MODE_WRITE),
	_msgType : null,
	_subject : null,
	_msgEditor : null,
	_sendBtn : null,
	_backBtn : null,
	_nextBtn : null,
	tr : butor.message.tr,
	construct : function(dlgOpts) {
		this.base(dlgOpts);
		this.setUrl('/message/newMessageDlg.html');
	},
	_init : function() {
		this.base();

		this._recipientBlock = this._jqe.find('#recipientBlock');
		this._preview = this._jqe.find('#preview');
		this._recipients = new butor.message.Recipient(this._recipientBlock, 
				butor.message._message._recipientTmpl);
		
		this._msgType = this._jqe.find('#msgType');
		this._loadMsgType();
		this._msgType.change($.proxy(this._msgTypeChanged, this));

		this._endDate = new butor.DatePicker(this._jqe.find('.endDateGroup'),
				{'errorMsg' : this.tr('End date is not valid!'), 'msgPanel' : this.getMsgPanel()});

		this._endTime = this._jqe.find('#endTime');
		this._loadEndTime();

		this._subject = this._jqe.find('#subject');

		this._msgEditor = this._jqe.find('#msgEditor');
		this._initEditor();
		
		this._backBtn = this._jqe.find('#backBtn').click($.proxy(this._back, this)).hide();
		this._nextBtn = this._jqe.find('#nextBtn').click($.proxy(this._next, this));
		this._sendBtn = this._jqe.find('#sendBtn').click($.proxy(this._attachAndMessage, this)).hide();
		
		this._closeBtn = this._jqe.find('#closeBtn').click($.proxy(function() {
			this.close();
		}, this));

		this._recipientBlock.delegate('.remove-message-recipient', 'click', function() {
			var rcpt = $(this);
			rcpt.parent().remove();
		});
		this._preview.delegate('.remove-message-recipient', 'click', function() {
			var rcpt = $(this);
			rcpt.parent().remove();
		});

		this._pageRemovedP = $.proxy(function(e) {
			if (e.data == 'message.sendMessage') {
				this._cleanup();
			}
		},this);
		App.bind('removing-page', this._pageRemovedP);

		this._langChangedP = $.proxy(function() {
			this._loadMsgType();
			var html = this._msgEditor.trumbowyg('html');
			this._msgEditor.trumbowyg('destroy');
			this._initEditor();
			this._msgEditor.trumbowyg('html', html);
		}, this);
		App.bind('langChanged', this._langChangedP);

		// attachments
		this._attachZone = this._jqe.find('#attachments');
		this._attachmentTmpl = this._attachZone.find('.attachment-item').remove();
		var self = this;
		
		this._dragLeaveTimer = null;
		// preventing page from redirecting
		this._jqe.on("dragover", function(e) {
			e.preventDefault();
			e.stopPropagation();
			if (self._dragLeaveTimer != null) {
				clearTimeout(self._dragLeaveTimer);
			} else {
				self._jqe.find('.attachments-screen').show();
			}
		});
		this._jqe.on("dragleave", function(e) {
			e.preventDefault();
			e.stopPropagation();
			
			this._dragLeaveTimer = setTimeout(function() {
				self._jqe.find('.attachments-screen').hide();
			}, 3000);
		});

		var checkAttachments = function() {
			var ail = self._attachZone.find('.attachment-item');
			if (ail[0])
				self._jqe.find('.no-attachments').hide();
			else
				self._jqe.find('.no-attachments').show();
		}
		this._jqe.on('drop', function(e) {
			e.stopPropagation();
			e.preventDefault();
			self._jqe.find('.attachments-screen').hide();

			var files = e.originalEvent.dataTransfer.files;
			for (var i = 0; i < files.length; i++) {
				var f = files[i];
				if (self._attachZone.find('[name="' +f['name'] +'"]')[0])
					// already attached
					continue;
				
				var item = self._attachmentTmpl.clone();
				item.data('file', f);
				item.attr('name', f['name']);
				item.find('.attachment-name').text(f['name']);
				item.find('.attachment-size').text(butor.message.formatAttachSize(f['size']));
				self._attachZone.append(item);
				item.show();
				item.find('.remove-attach-link').click(function() {
					var item = $(this).parent();
					item.remove();
					checkAttachments();
				});
			}
			checkAttachments();
		});

		
		this._reset();
		
		if (this._args && this._args['toFilter']) {
			this._msgType.val(this._args['msgType']).change();
			this._subject.val(this._args['subject']);
			if (this._args['msgType'] == 'alert') {
				this._endDate.setDate(this._args['endDate']);
				this._endTime.val(butor.Utils.formatTime(this._args['endDate'], false));
			}
			this._subject.val(this._args['subject']);
			var toFilters = ($.parseJSON(this._args['toFilter']) || {})['recipientFilterList']||[];
			for (var i=0; i<toFilters.length; i+=1) {
				var item = toFilters[i];
				var rcpt = butor.message._message._recipientTmpl.clone();
				var rcptId = rcpt.find('.message-recipient-id');
				var type = item['type'].toLowerCase();
				var th = butor.message._message.getRcptTypeHelper(type);
				rcptId.text(th != null ? th.get(item['id']) || item['id'] : item['id']);
				rcptId.attr('id', item['id']).attr('type', type);
				
				this._jqe.find('#' +type +'Recipients .message-recipients').append(rcpt);
			}
			this._msgEditor.trumbowyg('html', this._args['message']);
		}
	},
	_msgTypeChanged : function() {
		var mt = this._msgType.val();
		if (mt == 'alert') {
			this._jqe.find('.endDateTimeBlock').show();
		} else {
			this._endDate.setDate(null);
			this._jqe.find('.endDateTimeBlock').hide();
		}
	},
	_initEditor : function() {
		this._msgEditor.trumbowyg({
			'lang' : App.getLang(),
			'svgPath' : '/message/res/trumbowyg/ui/icons.svg',
			'btnsDef': {
                // Customizables dropdowns
                image: {
                    dropdown: ['insertImage', 'upload', 'base64', 'noembed'],
                    ico: 'insertImage'
                }
            },
			'btns': [
                   ['viewHTML'],
                   ['undo', 'redo'],
                   ['formatting'],
                   'btnGrp-design',
                   ['link'],
                   ['image'],
                   'btnGrp-justify',
                   'btnGrp-lists',
                   ['foreColor', 'backColor'],
                   ['preformatted'],
                   ['horizontalRule'],
                   ['fullscreen']
               ]
		}).attr('placeholder', this.tr('Compose a message'));
	},
	_loadMsgType : function() {
		var val = this._msgType.val();
		this._msgType.empty();
		this._msgType.append('<option value="">' +this.tr('--- select ---') +'</span></option>');
		this._msgType.append('<option value="info">' +this.tr('Information') +'</option>');
		this._msgType.append('<option value="alert">' +this.tr('Alert') +'</option>');
		this._msgType.val(val);
	},
	_loadEndTime : function() {
		this._endTime.empty();
		for (var i=0; i<24; i+=1) {
			var h = i;
			if (i<10) {
				h = butor.Utils.leftPad(i,2);
			}
			for (var j=0; j<60; j+=1) {
				if (j%15 == 0) {
					var m = j;
					if (m<10) {
						m = butor.Utils.leftPad(j,2);
					}
					var t = h+':'+m;
					var opt = $("<option>");
					opt.attr('value', t).text(t);
					this._endTime.append(opt);
				}
			}
		}
	},
	_cleanup : function() {
		App.unbind('removing-page', this._pageRemovedP);
		App.unbind('langChanged', this._langChangedP);
	},
	_reset : function() {
		this._msgType.val('');
		this._subject.val('');
		this._endDate.setDate(null);
		this._endTime.val('16:00');
		this._recipients.clear();
		this._preview.hide().empty();
		this._recipientBlock.show();
		this._msgEditor.val('');
		this._backBtn.hide();
		this._nextBtn.show();
		this._sendBtn.hide();
		this._jqe.find('.editable').butor('enable');
		this._msgEditor.trumbowyg('empty');
		this._msgEditor.trumbowyg('enable');
	},
	_destChanged : function() {
		this._jqe.find('.dest-block').hide();
		this._jqe.find('.' +this._toSB.val() +'-dest-block').show();
	},
	_back : function() {
		this._recipientBlock.show();
		this._preview.hide();
		this._backBtn.hide();
		this._nextBtn.show();
		this._sendBtn.hide();
		this._jqe.find('.editable').butor('enable');
		this._msgEditor.trumbowyg('enable');
	},
	_next : function() {
		this.hideMsg();
		this.mask(this.tr('Working'));

		if (this._msgEditor.closest('.trumbowyg-box').hasClass('trumbowyg-editor-hidden')) {
			this._msgEditor.trumbowyg('toggle');
		}
		
		var rfl = this._recipients.getData();
		
		var args = {'msgType': this._msgType.val(),
				'subject': this._subject.val(),
				'endDate': null,
				'recipientFilterList': rfl, 
				'message': this._msgEditor.trumbowyg('html')};
		if (args['msgType'] == 'alert') {
			args['endDate'] = butor.Utils.formatDate(this._endDate.getDate()) +' ' +this._endTime.val();
		}
		this._reqId = butor.message.MessageAjax.validateRecipients(args,
			$.proxy(function(resp) {
				this.unmask();
				if (!AJAX.isSuccessWithData(resp, this._reqId)) {
					return;
				}
				this._recipientBlock.hide();
				var rec = this._preview.show().empty();
				this._sendCriteria = resp.data[0];
				var recipients = this._sendCriteria.recipients;
				for (var i=0; i<recipients.length;i+=1) {
					var rcpt = butor.message._message._recipientTmpl.clone();
					rcpt.find('.message-recipient-id').text(recipients[i]);
					rec.append(rcpt);
				}
				
				this._backBtn.show();
				this._nextBtn.hide();
				this._sendBtn.show();
				this._jqe.find('.editable').butor('disable');
				this._msgEditor.trumbowyg('disable');
			}, this)
		);
	},
	_attachAndMessage : function() {
		this.hideMsg();
		this.mask(this.tr('Working'));
		var rcpts = []
		this._preview.find('.message-recipient-id').each(function() {
			rcpts.push($(this).text())
		});
		
		this._sendCriteria.recipients = rcpts;
		this._sendCriteria['msgType'] = this._msgType.find(':checked').val();
		this._sendCriteria['subject'] = this._subject.val();
		this._sendCriteria['message'] = this._msgEditor.trumbowyg('html');
		
		var ail = this._jqe.find('#attachments .attachment-item');
		if (ail[0]) {
			// attachments ?
			var attachments = [];
			var self = this;
			ail.each(function() {
				var item = $(this);
				var f = item.data('file');
				attachments.push({
					file:f,
					fileName:f['name'], 
					fileSize:f['size'], 
					contentType:f['type'], 
					contentBase64:null});
			});
			self._sendCriteria['attachments'] = attachments;
	
			var promises = attachments.map(function(attach) {
				return butor.message.fileToBase64(attach.file, function (b64) {
					if (b64 != null) {
						attach['contentBase64']=b64;
					}
				});
			});
	
			$.when.apply(null, promises).then(function() { 
				self._sendMessage(self._sendCriteria);
				return;
			});
		} else {
			this._sendMessage(this._sendCriteria);
		}
	},
	_sendMessage : function(criteria) {
		//TODO
		this._reqId = butor.message.MessageAjax.sendMessage(criteria,
			$.proxy(function(resp) {
				this.unmask();
				if (!AJAX.isSuccess(resp, this._reqId)) {
					return;
				}
				this.showMsg('Message sent');
				this.close();
			}, this)
		);
	}
});

butor.message.Recipient = butor.message.Recipient || butor.Class.define({
	_jqe : null,
	_rowEditor : null,
	_recipients : {},
	tr : butor.message.tr,
	construct : function(jqe, recipientTmpl) {
		this.base();
		this._jqe = jqe;
		this._recipientTmpl = recipientTmpl;
		
		this._recipientsTmpl = this._jqe.find('#recipientGroupTmpl').remove().removeAttr('id');

		this.configureRecipients();
	},
	configureRecipients : function() {
		var self = this;

		this._userRecipients = this._jqe.find('#userRecipients');
		var ulf = this._jqe.find('#userLookup');
		this._userlookup = new butor.sec.user.UserLookup(ulf);
		this._userlookup.bind('user-selected', function() {
			setTimeout(function() {
				var sel = self._userlookup.getSelection()||{};
				if (sel && sel['id']) {
					var doneSel = self._userRecipients.find('.message-recipient-id[id="' +sel['id'] +'"]');
					if (doneSel[0]) {
						doneSel.parent().effect("pulsate", { times:2 }, 1000);
						return;
					}
					var rcpt = self._recipientTmpl.clone();
					rcpt.find('.message-recipient-id').text(sel['id']).attr('id', sel['id']).attr('type', 'user');
					rcpt.insertAfter(ulf);
					self._userlookup.clear();
				}
			}, 100);
		});

		this._prepRcptGroup('branch');
		this._prepRcptGroup('firm');
		this._prepRcptGroup('group');
		this._prepRcptGroup('role');
		this._prepRcptGroup('system');
	},
	_prepRcptGroup : function(type) {
		new function(self, type) {
			var id = type +'Recipients';
			var rcptGrp = self._recipientsTmpl.clone().attr('id', id);
			rcptGrp.find('.recipient-group-label').text(self.tr(type));
			self._jqe.append(rcptGrp);
	
			var dd = rcptGrp.find('ul.dropdown-menu').empty();
			var rth = butor.message._message.getRcptTypeHelper(type);
			var il = [];
			if (rth) {
				il = rth.list();
			}
			for (var i=0; i<il.length; i+=1) {
				var item = il[i];
				var ii = $('<li><a href="#"></a></li>');
				ii.find('a').attr('id', item['k1']).text(item['value']);
				dd.append(ii);
			}
			dd.find('a').click(function() {
				var item = $(this);
				var id = item.attr('id');
				var doneSel = rcptGrp.find('.message-recipient-id[id="' +id +'"]');
				if (doneSel[0]) {
					doneSel.parent().effect("pulsate", { times:2 }, 1000);
					return;
				}
				setTimeout(function() {
					var rcpt = self._recipientTmpl.clone();
					var rth = butor.message._message.getRcptTypeHelper(type);
					rcpt.find('.message-recipient-id').text(rth ? rth.get(id) : id).attr('id', id).attr('type', type);
					rcptGrp.find('.message-recipients').prepend(rcpt);
				}, 100);
			});
		}(this, type);
	},
	clear : function() {
		this._jqe.find('div.message-recipient').remove();
	},

	getData : function() {
		var data = [];
		this._jqe.find('.message-recipient-id').each(function(index, elm) {
			var rcpt = $(this);
			data.push({'type':rcpt.attr('type').toUpperCase(), 'id':rcpt.attr('id')}); 
		});
		return data;
	}
});

//# sourceURL=butor.message.message.js
