/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.message.common.message;

import java.util.Date;
import java.util.List;

public class Message {
	private Long id;
	private Long contentId;
	private Long relatedTo;
	private String toUserId; 
	private String fromUserId;
	private String fromUserDisplayName;
	private String msgType;
	private int nbAttach;
	private String subject;
	private String message;
	private String toFilter;
	private int status;
	private Date creationDate;
	private Date endDate;
	private Date readDate;
	private Date stamp;
	private int revNo;
	private String userId;
	
	List<MessageAttach> attachments;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRelatedTo() {
		return relatedTo;
	}
	public void setRelatedTo(Long relatedTo) {
		this.relatedTo = relatedTo;
	}
	public String getToUserId() {
		return toUserId;
	}
	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}
	public String getFromUserId() {
		return fromUserId;
	}
	public void setFromUserId(String fromUserId) {
		this.fromUserId = fromUserId;
	}
	
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getReadDate() {
		return readDate;
	}
	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}
	public Date getStamp() {
		return stamp;
	}
	public void setStamp(Date stamp) {
		this.stamp = stamp;
	}
	public int getRevNo() {
		return revNo;
	}
	public void setRevNo(int revNo) {
		this.revNo = revNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attachments == null) ? 0 : attachments.hashCode());
		result = prime * result + ((contentId == null) ? 0 : contentId.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((fromUserDisplayName == null) ? 0 : fromUserDisplayName.hashCode());
		result = prime * result + ((fromUserId == null) ? 0 : fromUserId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((msgType == null) ? 0 : msgType.hashCode());
		result = prime * result + nbAttach;
		result = prime * result + ((readDate == null) ? 0 : readDate.hashCode());
		result = prime * result + ((relatedTo == null) ? 0 : relatedTo.hashCode());
		result = prime * result + revNo;
		result = prime * result + ((stamp == null) ? 0 : stamp.hashCode());
		result = prime * result + status;
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((toFilter == null) ? 0 : toFilter.hashCode());
		result = prime * result + ((toUserId == null) ? 0 : toUserId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (attachments == null) {
			if (other.attachments != null)
				return false;
		} else if (!attachments.equals(other.attachments))
			return false;
		if (contentId == null) {
			if (other.contentId != null)
				return false;
		} else if (!contentId.equals(other.contentId))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (fromUserDisplayName == null) {
			if (other.fromUserDisplayName != null)
				return false;
		} else if (!fromUserDisplayName.equals(other.fromUserDisplayName))
			return false;
		if (fromUserId == null) {
			if (other.fromUserId != null)
				return false;
		} else if (!fromUserId.equals(other.fromUserId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (msgType == null) {
			if (other.msgType != null)
				return false;
		} else if (!msgType.equals(other.msgType))
			return false;
		if (nbAttach != other.nbAttach)
			return false;
		if (readDate == null) {
			if (other.readDate != null)
				return false;
		} else if (!readDate.equals(other.readDate))
			return false;
		if (relatedTo == null) {
			if (other.relatedTo != null)
				return false;
		} else if (!relatedTo.equals(other.relatedTo))
			return false;
		if (revNo != other.revNo)
			return false;
		if (stamp == null) {
			if (other.stamp != null)
				return false;
		} else if (!stamp.equals(other.stamp))
			return false;
		if (status != other.status)
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (toFilter == null) {
			if (other.toFilter != null)
				return false;
		} else if (!toFilter.equals(other.toFilter))
			return false;
		if (toUserId == null) {
			if (other.toUserId != null)
				return false;
		} else if (!toUserId.equals(other.toUserId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Message [id=" + id + ", contentId=" + contentId + ", relatedTo=" + relatedTo + ", toUserId=" + toUserId
				+ ", fromUserId=" + fromUserId + ", fromUserDisplayName=" + fromUserDisplayName + ", msgType=" + msgType
				+ ", nbAttach=" + nbAttach + ", subject=" + subject + ", message=" + message + ", toFilter=" + toFilter
				+ ", status=" + status + ", creationDate=" + creationDate + ", endDate=" + endDate + ", readDate="
				+ readDate + ", stamp=" + stamp + ", revNo=" + revNo + ", userId=" + userId + ", attachments="
				+ attachments + "]";
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getToFilter() {
		return toFilter;
	}
	public void setToFilter(String toFilter) {
		this.toFilter = toFilter;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Long getContentId() {
		return contentId;
	}
	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}
	public List<MessageAttach> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<MessageAttach> attachments) {
		this.attachments = attachments;
	}
	public int getNbAttach() {
		return nbAttach;
	}
	public void setNbAttach(int nbAttach) {
		this.nbAttach = nbAttach;
	}
	public String getFromUserDisplayName() {
		return fromUserDisplayName;
	}
	public void setFromUserDisplayName(String fromUserDisplayName) {
		this.fromUserDisplayName = fromUserDisplayName;
	}
}