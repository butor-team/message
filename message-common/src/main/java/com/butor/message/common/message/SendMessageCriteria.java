/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package com.butor.message.common.message;

import java.util.List;

/**
 * @author asawan
 *
 */
public class SendMessageCriteria {
	private List<RecipientFilter> recipientFilterList = null;
	private List<String> recipients;
	private String msgType;
	private String subject;
	private String message;
	private String endDate;
	private List<MessageAttach> attachments;

	public List<RecipientFilter> getRecipientFilterList() {
		return recipientFilterList;
	}

	public void setRecipientFilterList(List<RecipientFilter> recipientCriteriaList) {
		this.recipientFilterList = recipientCriteriaList;
	}

	@Override
	public String toString() {
		return "SendMessageCriteria [recipientFilterList=" + recipientFilterList + ", recipients=" + recipients
				+ ", msgType=" + msgType + ", subject=" + subject + ", message=" + message + ", endDate=" + endDate
				+ ", attachments=" + attachments + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attachments == null) ? 0 : attachments.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((msgType == null) ? 0 : msgType.hashCode());
		result = prime * result + ((recipientFilterList == null) ? 0 : recipientFilterList.hashCode());
		result = prime * result + ((recipients == null) ? 0 : recipients.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SendMessageCriteria other = (SendMessageCriteria) obj;
		if (attachments == null) {
			if (other.attachments != null)
				return false;
		} else if (!attachments.equals(other.attachments))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (msgType == null) {
			if (other.msgType != null)
				return false;
		} else if (!msgType.equals(other.msgType))
			return false;
		if (recipientFilterList == null) {
			if (other.recipientFilterList != null)
				return false;
		} else if (!recipientFilterList.equals(other.recipientFilterList))
			return false;
		if (recipients == null) {
			if (other.recipients != null)
				return false;
		} else if (!recipients.equals(other.recipients))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}

	public List<String> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public List<MessageAttach> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<MessageAttach> attachments) {
		this.attachments = attachments;
	}
}
