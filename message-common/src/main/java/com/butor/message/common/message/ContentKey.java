/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.message.common.message;

public class ContentKey {
	private long contentId;
	private int revNo;
	
	public ContentKey(long contentId, int revNo) {
		super();
		this.contentId = contentId;
		this.revNo = revNo;
	}

	public long getContentId() {
		return contentId;
	}

	public void setContentId(long contentId) {
		this.contentId = contentId;
	}

	public int getRevNo() {
		return revNo;
	}

	public void setRevNo(int revNo) {
		this.revNo = revNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (contentId ^ (contentId >>> 32));
		result = prime * result + revNo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContentKey other = (ContentKey) obj;
		if (contentId != other.contentId)
			return false;
		if (revNo != other.revNo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MessageKey [contentId=" + contentId + ", revNo=" + revNo + "]";
	}
	
}
