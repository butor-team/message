/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package com.butor.message.common.message;

import java.util.List;

/**
 * @author asawan
 *
 */
public class MessageKeyList {
	private List<MessageKey> keyList;

	public List<MessageKey> getKeyList() {
		return keyList;
	}

	public void setKeyList(List<MessageKey> keyList) {
		this.keyList = keyList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((keyList == null) ? 0 : keyList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageKeyList other = (MessageKeyList) obj;
		if (keyList == null) {
			if (other.keyList != null)
				return false;
		} else if (!keyList.equals(other.keyList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MessageKeyList [keyList=" + keyList + "]";
	}
}
