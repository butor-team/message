/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package com.butor.message.common.message;

/**
 * @author asawan
 *
 */
public class MessageAttach {
	private Long fileId;
	private long messageId;
	private long contentId;
	private int fileSize;
	private String fileName;
	private String contentType;
	private String contentBase64;

	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public long getContentId() {
		return contentId;
	}
	public void setContentId(long contentId) {
		this.contentId = contentId;
	}
	public int getFileSize() {
		return fileSize;
	}
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (contentId ^ (contentId >>> 32));
		result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + ((contentBase64 == null) ? 0 : contentBase64.hashCode());
		result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + fileSize;
		result = prime * result + (int) (messageId ^ (messageId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageAttach other = (MessageAttach) obj;
		if (contentId != other.contentId)
			return false;
		if (contentType == null) {
			if (other.contentType != null)
				return false;
		} else if (!contentType.equals(other.contentType))
			return false;
		if (contentBase64 == null) {
			if (other.contentBase64 != null)
				return false;
		} else if (!contentBase64.equals(other.contentBase64))
			return false;
		if (fileId == null) {
			if (other.fileId != null)
				return false;
		} else if (!fileId.equals(other.fileId))
			return false;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (fileSize != other.fileSize)
			return false;
		if (messageId != other.messageId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "MessageAttach [fileId=" + fileId + ", messageId=" + messageId + ", contentId=" + contentId
				+ ", fileSize=" + fileSize + ", fileName=" + fileName + ", contentType=" + contentType
				+ ", contentBase64=" + contentBase64 + "]";
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getContentBase64() {
		return contentBase64;
	}
	public void setContentBase64(String contentBase64) {
		this.contentBase64 = contentBase64;
	}
}
