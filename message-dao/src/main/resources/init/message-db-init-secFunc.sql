--
-- Copyright 2013-2018 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

# security functions ---------------
INSERT INTO `secFunc` (`func`, `sys`, `description`, `stamp`, `userId`,`revNo`) VALUES 
	('message', 'message', 'Read messages', NOW(), 'load',0),
	('sendMessage', 'message', 'Send messages', NOW(), 'load',0)
;

# attrSet -------------------------------
# systems
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES
	('codeset','systems','message', 1, 'fr', 'Messages', NOW(), 'load',0),
	('codeset','systems','message', 1, 'en', 'Messages', NOW(), 'load',0)
;
