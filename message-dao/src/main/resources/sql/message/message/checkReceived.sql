--
-- Copyright 2013-2018 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

select count(*) from (

select
  m.id
from ${security.dbName:PORTAL}.message m
inner join ${security.dbName:PORTAL}.messageContent c on c.contentId = m.contentId
where
  (:toUserId is null and m.toUserId = :member) and
  m.status = 0 and
  (:msgType is null or c.msgType = :msgType)


union

select
  m.id
from ${security.dbName:PORTAL}.message m
inner join ${security.dbName:PORTAL}.messageContent c on c.contentId = m.contentId
inner join (__authDataSql__) d on
	(d.dataType = 'user' and
	(d.d1 = '*' or d.d1 = m.toUserId)) 
where
  (m.toUserId = :toUserId) and
  m.status = 0 and
  (:msgType is null or c.msgType = :msgType)

) x