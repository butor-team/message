--
-- Copyright 2013-2018 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

select distinct * from 
(
select
  m.id,
  m.contentId,
  s.relatedTo,
  m.toUserId, 
  s.fromUserId,
  su.displayName as fromUserDisplayName,
  
  c.msgType,
  c.subject,
  if(:msgType='alert', if(:status=0, c.message, null), null) as message,
  ifnull(a.nbAttach, 0) as nbAttach,
  
  c.creationDate,
  c.endDate,
  m.status,
  m.readDate,
  m.revNo,
  m.stamp,
  m.userId
from ${security.dbName:PORTAL}.message m
inner join ${security.dbName:PORTAL}.messageContent c on c.contentId = m.contentId
inner join ${security.dbName:PORTAL}.sentMessage s on s.contentId = m.contentId
left join ${security.dbName:PORTAL}.secUser su on su.id = s.fromUserId
left join (
	select a.contentId, count(a.fileId) as nbAttach
	from ${security.dbName:PORTAL}.messageAttach a
	group by a.contentId
) a on a.contentId = m.contentId
	
where
  (:toUserId is null and m.toUserId = :member) and
  (:msgType is null or c.msgType = :msgType) and
  ((:status is null and m.status < 2) or (m.status = :status))

union

select
  m.id,
  m.contentId,
  s.relatedTo,
  m.toUserId, 
  s.fromUserId,
  su.displayName as fromUserDisplayName,
  
  c.msgType,
  c.subject,
  if(:msgType='alert', if(:status=0, c.message, null), null) as message,
  ifnull(a.nbAttach, 0) as nbAttach,

  c.creationDate,
  c.endDate,
  m.status,
  m.readDate,
  m.revNo,
  m.stamp,
  m.userId
from ${security.dbName:PORTAL}.message m
inner join ${security.dbName:PORTAL}.messageContent c on c.contentId = m.contentId
inner join ${security.dbName:PORTAL}.sentMessage s on s.contentId = m.contentId
left join ${security.dbName:PORTAL}.secUser su on su.id = s.fromUserId
inner join (__authDataSql__) d on
  (d.dataType = 'user' and
  (d.d1 = '*' or d.d1 = m.toUserId)) 
left join (
	select a.contentId, count(a.fileId) as nbAttach
	from ${security.dbName:PORTAL}.messageAttach a
	group by a.contentId
) a on a.contentId = m.contentId

where
  (m.toUserId = :toUserId) and
  (:msgType is null or c.msgType = :msgType) and
  ((:status is null and m.status < 2) or (m.status = :status))

) x

order by creationDate desc
