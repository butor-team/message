--
-- Copyright 2013-2018 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

select
  m.id,
  m.contentId,
  m.relatedTo,
  m.toFilter, 
  m.fromUserId,
  
  c.msgType,
  c.subject,
  c.message,
  c.creationDate,
  c.endDate,
  
  m.revNo,
  m.stamp,
  m.userId
from ${security.dbName:PORTAL}.sentMessage m
inner join ${security.dbName:PORTAL}.messageContent c on c.contentId = m.contentId
where
  m.fromUserId = :userId and 
  m.id = :id

