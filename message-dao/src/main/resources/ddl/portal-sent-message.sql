--
-- Copyright 2013-2018 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

/* 2019-09-26
ALTER TABLE sentMessage
  DROP COLUMN msgType,
  DROP COLUMN subject,
  DROP COLUMN message,
  DROP COLUMN creationDate,
  DROP COLUMN endDate,
  DROP COLUMN status,
  add column contentId bigint comment 'message details id' after id;
*/

DROP TABLE IF EXISTS sentMessage;

CREATE TABLE sentMessage (
  id bigint not null comment 'message Id' auto_increment,
  contentId bigint comment 'message details id',
  relatedTo bigint comment 'Related To',
  toFilter mediumtext not null comment 'To recipients filter', 
  fromUserId varchar(128) not null comment 'From User Id',
  revNo integer not null comment 'Revision Number',
  stamp timestamp not null default CURRENT_TIMESTAMP comment 'Last Modification Timestamp',
  userId varchar(250) not null comment 'Last Modification User Id',

PRIMARY KEY (id),
INDEX Xcd (creationDate, endDate),
INDEX Xfrom (fromUserId)
)
ENGINE = InnoDB;


