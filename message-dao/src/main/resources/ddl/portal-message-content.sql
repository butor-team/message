--
-- Copyright 2013-2018 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

DROP TABLE IF EXISTS messageContent;

CREATE TABLE messageContent (
  contentId bigint not null comment 'message Id' auto_increment,
  msgType varchar(50) not null comment 'Message Type',
  subject varchar(256) not null comment 'message Subject',
  message mediumtext not null comment 'Message Body',
  creationDate datetime not null comment 'Creation Date',
  endDate datetime comment 'end date of alert',
  revNo integer not null comment 'Revision Number',
  stamp timestamp not null default CURRENT_TIMESTAMP comment 'Last Modification Timestamp',
  userId varchar(250) not null comment 'Last Modification User Id',

PRIMARY KEY (contentId)
)
ENGINE = InnoDB;


DROP TABLE IF EXISTS messageAttach;

CREATE TABLE messageAttach (
  fileId bigint not null comment 'File Id' auto_increment,
  contentId bigint not null comment 'Message Content Id',
  fileName varchar(255) not null comment 'File name',
  fileSize integer not null comment 'File size',
  contentType varchar(255) not null comment 'File content type',
  contentBase64 MEDIUMBLOB not null comment 'File content of max length of 16M',

PRIMARY KEY (fileId),
INDEX (contentId, fileId)
)
ENGINE = InnoDB;
