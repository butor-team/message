/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.message.dao.message;


import java.util.List;

import org.butor.json.CommonRequestArgs;

import com.butor.message.common.message.ListMessageCriteria;
import com.butor.message.common.message.Message;
import com.butor.message.common.message.MessageKey;

public interface MessageDao {

	int checkMailReceived(ListMessageCriteria criteria, CommonRequestArgs cra);

	List<Message> listMessageReceived(ListMessageCriteria criteria, CommonRequestArgs cra);
	
	Message readMessage(long id, CommonRequestArgs cra);

	MessageKey insertMessage(Message message, CommonRequestArgs cra);
	void insertDeletedMessage(Message message, CommonRequestArgs cra);

	MessageKey updateMessage(Message message, CommonRequestArgs cra);

	void deleteMessage(MessageKey key, CommonRequestArgs cra);
}
