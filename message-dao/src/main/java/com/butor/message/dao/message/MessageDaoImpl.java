/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.message.dao.message;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.butor.attrset.dao.AttrSetDaoImpl;
import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.dao.AuthDao;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.butor.message.common.MessageConstants;
import com.butor.message.common.message.ListMessageCriteria;
import com.butor.message.common.message.Message;
import com.butor.message.common.message.MessageKey;

public class MessageDaoImpl extends AbstractDao implements MessageDao {
	protected static Logger logger = LoggerFactory.getLogger(AttrSetDaoImpl.class);

	private final String PROC_CHECK_MESSAGE_RECEIVED = getClass().getName() +".checkReceived";
	private final String PROC_LIST_MESSAGE_RECEIVED = getClass().getName() +".listReceived";
	private final String PROC_READ_MESSAGE = getClass().getName() +".readMessage";
	private final String PROC_INSERT_MESSAGE = getClass().getName() +".insertMessage";
	private final String PROC_INSERT_DELETED_MESSAGE = getClass().getName() +".insertDeletedMessage";
	private final String PROC_UPDATE_MESSAGE = getClass().getName() +".updateMessage";
	private final String PROC_DELETE_MESSAGE = getClass().getName() +".deleteMessage";

	private String checkReceivedSql;
	private String listReceivedSql;
	private String readMessageSql;
	private String insertMessageSql;
	private String insertDeletedMessageSql;
	private String updateMessageSql;
	private String deleteMessageSql;
	private AuthDao authDao;

	@Override
	public int checkMailReceived(ListMessageCriteria criteria, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
				MessageConstants.SEC_FUNC_MESSAGE, AccessMode.READ, cra)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}

		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(MessageConstants.SYSTEM_ID).
		setMode(AccessMode.READ.value()).
		setFunc(MessageConstants.SEC_FUNC_MESSAGE).
		setDataTypes("user"). 
		setMember(cra.getUserId());
		
		return queryForInt(PROC_CHECK_MESSAGE_RECEIVED, checkReceivedSql, criteria, authData, cra);
	}

	@Override
	public List<Message> listMessageReceived(ListMessageCriteria criteria, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
				MessageConstants.SEC_FUNC_MESSAGE, AccessMode.READ, cra)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}

		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(MessageConstants.SYSTEM_ID).
		setMode(AccessMode.READ.value()).
		setFunc(MessageConstants.SEC_FUNC_MESSAGE).
		setDataTypes("user"). 
		setMember(cra.getUserId());
		
		return queryList(PROC_LIST_MESSAGE_RECEIVED, listReceivedSql, Message.class, criteria, authData, cra);
	}

	@Override
	public Message readMessage(long id, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
				MessageConstants.SEC_FUNC_MESSAGE, AccessMode.READ, cra)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}

		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(MessageConstants.SYSTEM_ID).
		setMode(AccessMode.READ.value()).
		setFunc(MessageConstants.SEC_FUNC_MESSAGE).
		setDataTypes("user"). 
		setMember(cra.getUserId());

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", id);
		
		return queryFirst(PROC_READ_MESSAGE, readMessageSql, Message.class, args, authData, cra);
	}

	@Override
	public MessageKey insertMessage(Message message, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
				MessageConstants.SEC_FUNC_SEND_MESSAGE, AccessMode.WRITE, cra)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}

		message.setRevNo(0);
		UpdateResult ur = insert(PROC_INSERT_MESSAGE, insertMessageSql, message, cra);
		if (ur.numberOfRowAffected <= 0) {
			throw ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage()); 
		}
		
		return new MessageKey(ur.key, message.getRevNo());
	}
	@Override
	public void insertDeletedMessage(Message message, CommonRequestArgs cra) {
		UpdateResult ur = insert(PROC_INSERT_DELETED_MESSAGE, insertDeletedMessageSql, message, cra);
		if (ur.numberOfRowAffected <= 0) {
			throw ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage()); 
		}
	}

	@Override
	public MessageKey updateMessage(Message message, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
				MessageConstants.SEC_FUNC_MESSAGE, AccessMode.WRITE, cra)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}

		UpdateResult ur = update(PROC_UPDATE_MESSAGE, updateMessageSql, message, cra);
		if (ur.numberOfRowAffected == 0) {
			throw ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage()); 
		} 
		return new MessageKey(message.getId(), message.getRevNo());

	}

	@Override
	public void deleteMessage(MessageKey key, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
				MessageConstants.SEC_FUNC_MESSAGE, AccessMode.WRITE, cra)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}

		UpdateResult ur = delete(PROC_DELETE_MESSAGE, deleteMessageSql, key ,cra);
		if (ur.numberOfRowAffected == 0) {
			throw ApplicationException.exception(DAOMessageID.DELETE_FAILURE.getMessage()); 
		} 
	}

	public void setListReceivedSql(String listReceivedSql) {
		this.listReceivedSql = listReceivedSql;
	}

	public void setReadMessageSql(String readMailSql) {
		this.readMessageSql = readMailSql;
	}

	public void setInsertMessageSql(String insertMailSql) {
		this.insertMessageSql = insertMailSql;
	}

	public void setUpdateMessageSql(String updateMailSql) {
		this.updateMessageSql = updateMailSql;
	}

	public void setDeleteMessageSql(String deleteMailSql) {
		this.deleteMessageSql = deleteMailSql;
	}

	public void setAuthDao(AuthDao authDao) {
		this.authDao = authDao;
	}

	public void setCheckReceivedSql(String checkReceivedSql) {
		this.checkReceivedSql = checkReceivedSql;
	}

	public void setInsertDeletedMessageSql(String insertDeletedMessageSql) {
		this.insertDeletedMessageSql = insertDeletedMessageSql;
	}
}