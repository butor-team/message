/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.message.dao.message;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.butor.attrset.dao.AttrSetDaoImpl;
import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.dao.AuthDao;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.ArgsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.butor.message.common.MessageConstants;
import com.butor.message.common.message.ContentKey;
import com.butor.message.common.message.ListMessageCriteria;
import com.butor.message.common.message.Message;
import com.butor.message.common.message.MessageAttach;
import com.butor.message.common.message.MessageKey;

public class SentMessageDaoImpl extends AbstractDao implements SentMessageDao {
	protected static Logger logger = LoggerFactory.getLogger(AttrSetDaoImpl.class);

	private final String PROC_LIST_MESSAGE = getClass().getName() +".listMessage";
	private final String PROC_READ_MESSAGE = getClass().getName() +".readMessage";
	private final String PROC_INSERT_MESSAGE = getClass().getName() +".insertMessage";
	private final String PROC_INSERT_CONTENT = getClass().getName() +".insertContent";
	private final String PROC_DELETE_MESSAGE = getClass().getName() +".deleteMessage";

	private final String PROC_LIST_ATTACH = getClass().getName() +".listAttach";
	private final String PROC_READ_ATTACH = getClass().getName() +".readAttach";
	private final String PROC_INSERT_ATTACH = getClass().getName() +".insertAttach";

	private String listSql;
	private String readSql;
	private String insertSql;
	private String insertContentSql;
	private String deleteSql;

	private String listAttachSql;
	private String readAttachSql;
	private String insertAttachSql;

	private AuthDao authDao;

	@Override
	public List<Message> listMessage(ListMessageCriteria criteria, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
				MessageConstants.SEC_FUNC_MESSAGE, AccessMode.READ, cra)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}

		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(MessageConstants.SYSTEM_ID).
		setMode(AccessMode.READ.value()).
		setFunc(MessageConstants.SEC_FUNC_MESSAGE).
		setDataTypes("user"). 
		setMember(cra.getUserId());
		
		return queryList(PROC_LIST_MESSAGE, listSql, Message.class, criteria, authData, cra);
	}

	@Override
	public Message readMessage(long id, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
				MessageConstants.SEC_FUNC_MESSAGE, AccessMode.READ, cra)) {
				ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
			}

		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(MessageConstants.SYSTEM_ID).
		setMode(AccessMode.READ.value()).
		setFunc(MessageConstants.SEC_FUNC_MESSAGE).
		setDataTypes("user"). 
		setMember(cra.getUserId());

		ArgsBuilder ab = ArgsBuilder.create().set("id", id);
		
		Message msg = queryFirst(PROC_READ_MESSAGE, readSql, Message.class, ab.build(), authData, cra);
		msg.setAttachments(listAttach(msg.getContentId(), cra));
		
		return msg;
	}

	@Override
	public MessageKey insertMessage(Message message, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
			MessageConstants.SEC_FUNC_SEND_MESSAGE, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		message.setRevNo(0);
		UpdateResult ur = insert(PROC_INSERT_MESSAGE, insertSql, message, cra);
		if (ur.numberOfRowAffected <= 0) {
			throw ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage()); 
		}
		
		return new MessageKey(ur.key, message.getRevNo());
	}

	@Override
	public ContentKey insertContent(Message message, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
			MessageConstants.SEC_FUNC_SEND_MESSAGE, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		message.setRevNo(0);
		UpdateResult ur = insert(PROC_INSERT_CONTENT, insertContentSql, message, cra);
		if (ur.numberOfRowAffected <= 0) {
			throw ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage()); 
		}
		
		ContentKey ck = new ContentKey(ur.key, message.getRevNo());
		message.setContentId(ck.getContentId());
		
		return ck;
	}
	@Override
	public void deleteMessage(MessageKey key, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
			MessageConstants.SEC_FUNC_MESSAGE, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		UpdateResult ur = delete(PROC_DELETE_MESSAGE, deleteSql, key ,cra);
		if (ur.numberOfRowAffected == 0) {
			throw ApplicationException.exception(DAOMessageID.DELETE_FAILURE.getMessage()); 
		}
	}

	public List<MessageAttach> listAttach(long messageId, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
			MessageConstants.SEC_FUNC_MESSAGE, AccessMode.READ, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}
		
		ArgsBuilder ab = ArgsBuilder.create().set("contentId", messageId);
		return queryList(PROC_LIST_ATTACH, listAttachSql, MessageAttach.class, ab.build(), cra);
	}

	@Override
	public MessageAttach readAttach(long contentId, long fileId, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID, 
			MessageConstants.SEC_FUNC_MESSAGE, AccessMode.READ, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		ArgsBuilder ab = ArgsBuilder.create().set("contentId", contentId).set("fileId", fileId);
		
		return queryFirst(PROC_READ_ATTACH, readAttachSql, MessageAttach.class, ab.build(), cra);
	}

	@Override
	public long insertAttach(MessageAttach messageAttach, CommonRequestArgs cra) {
		if (!authDao.hasAccess(MessageConstants.SYSTEM_ID,
			MessageConstants.SEC_FUNC_SEND_MESSAGE, AccessMode.WRITE, cra)) {
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		UpdateResult ur = insert(PROC_INSERT_ATTACH, insertAttachSql, messageAttach, cra);
		if (ur.numberOfRowAffected <= 0) {
			throw ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage()); 
		}
		
		return ur.key;
	}

	public void setAuthDao(AuthDao authDao) {
		this.authDao = authDao;
	}

	public void setListSql(String listSql) {
		this.listSql = listSql;
	}

	public void setReadSql(String readSql) {
		this.readSql = readSql;
	}

	public void setInsertSql(String insertSql) {
		this.insertSql = insertSql;
	}

	public void setDeleteSql(String deleteSql) {
		this.deleteSql = deleteSql;
	}

	public void setInsertContentSql(String insertContentSql) {
		this.insertContentSql = insertContentSql;
	}

	public void setListAttachSql(String listAttachSql) {
		this.listAttachSql = listAttachSql;
	}

	public void setReadAttachSql(String readAttachSql) {
		this.readAttachSql = readAttachSql;
	}

	public void setInsertAttachSql(String insertAttachSql) {
		this.insertAttachSql = insertAttachSql;
	}
}