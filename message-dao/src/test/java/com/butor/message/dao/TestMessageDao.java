/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.message.dao;


import static org.butor.test.ButorTestUtil.getCommonRequestArgs;

import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;

import com.butor.message.common.message.ListMessageCriteria;
import com.butor.message.common.message.Message;
import com.butor.message.common.message.MessageKey;
import com.butor.message.dao.message.MessageDao;

@RunWith(value=SpringJUnit4ClassRunner.class)
@ContextConfiguration(value="classpath:test-context.xml")
public class TestMessageDao {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="jdbcDriverType")
	String jdbcDriverType;

	@Resource(name="messageDs")
	DataSource dataSource;

	@Resource
	MessageDao messageDao;

	ButorTestUtil testUtil = new ButorTestUtil();

	@Before 
	public void init() throws Exception{
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		testUtil.executeSQL(jt, ResourceUtils.getURL("classpath:ddl/portal-message.sql"), "TESTPORTAL", "mysql");


		testUtil.executeSQL(jt, ResourceUtils.getURL("classpath:message-portal-initTest.sql"), "TESTPORTAL", "mysql");
		testUtil.executeSQL(jt, ResourceUtils.getURL("classpath:TestMessageDao.sql"), "TESTPORTAL", "mysql");
	}

	
	@Test
	public void test() {
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");

		ListMessageCriteria crit = new ListMessageCriteria();
		crit.setToUserId("TEST");
		List<Message> listMail = messageDao.listMessageReceived(crit, cra);
		Assert.assertNotNull(listMail);
		Assert.assertEquals(2, listMail.size());

		Assert.assertTrue(1 <= listMail.size());
		
		Message message = listMail.get(0);

		
		long id = message.getId();
		MessageKey key;
		
		message = messageDao.readMessage(id, cra);
		Assert.assertNotNull(message);
		
		key = messageDao.insertMessage(message, cra);
		Assert.assertNotNull(key);

		message = messageDao.readMessage(key.getId(), cra);
		Assert.assertNotNull(message);
		Assert.assertEquals("Subject Test 1", message.getSubject());

		message.setSubject("Subject Test X");
		key = messageDao.updateMessage(message, cra);
		Assert.assertNotNull(key);
		
		message = messageDao.readMessage(key.getId(), cra);
		Assert.assertNotNull(message);
		Assert.assertEquals("Subject Test X", message.getSubject());
		
		key = new MessageKey(message.getId(), message.getRevNo());
		messageDao.deleteMessage(key, cra);
		messageDao.insertDeletedMessage(message, cra);

		message = messageDao.readMessage(key.getId(), cra);
		Assert.assertNull(message);
	}
	

	@Test
	public void testListMailReceived() {
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		
		ListMessageCriteria criteria = new ListMessageCriteria();

		criteria.setToUserId("TEST");
		List<Message> listMailReceived = messageDao.listMessageReceived(criteria, cra);
		Assert.assertEquals(2, listMailReceived.size());
	}
	@Test
	public void testCheckMailReceived() {
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		
		ListMessageCriteria criteria = new ListMessageCriteria();

		criteria.setToUserId("TEST");
		int count = messageDao.checkMailReceived(criteria, cra);
		Assert.assertEquals(2, count);
	}
}
