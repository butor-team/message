--
-- Copyright 2013-2018 butor.com
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

DELETE FROM `TESTPORTAL`.`message`;
INSERT INTO `TESTPORTAL`.`message` 
(`id`, `msgType`, `relatedTo`, `toUserId`, `fromUserId`, `subject`, `message`, `creationDate`, `status`, `readDate`, `revNo`, `stamp`, `userId`) 
VALUES 
(null, 'info', null, 'TEST', 'TST2', 'Subject Test 1', 'Message Test 1', NOW(), 0, null, '0', CURRENT_TIMESTAMP, 'unittest'),
(null, 'info', null, 'TEST', 'TST3', 'Subject Test 2', 'Message Test 2', NOW(), 0, null, '0', CURRENT_TIMESTAMP, 'unittest'),
(null, 'info', null, 'TST2', 'TEST', 'Subject Test 3', 'Message Test 3', NOW(), 0, null, '0', CURRENT_TIMESTAMP, 'unittest'),
(null, 'info', null, 'TST2', 'TEST', 'Subject Test 4', 'Message Test 4', NOW(), 0, null, '0', CURRENT_TIMESTAMP, 'unittest'),
(null, 'info', null, 'TST3', 'TST2', 'Subject Test 5', 'Message Test 5', NOW(), 0, null, '0', CURRENT_TIMESTAMP, 'unittest')

;